﻿using UnityEngine;
using System.Collections;

public class Player : MonoBehaviour 
{

	public float speed;
	public float jumpSpeed;
	public bool isGround;
	public GameObject bullet;
	public float bulletSpeed = 20000f;
	public GameObject bulletSpawn;

	private HingeJoint2D shoulder;
	private float angle;
	private float delay;
	private Vector3 mousePos;
	private GameObject arm;
	private Rigidbody2D rb;

	// Use this for initialization
	void Awake () 
	{
		shoulder = GetComponentInChildren<HingeJoint2D> ();
		arm = GameObject.Find ("Arms");
		bulletSpawn = GameObject.Find ("bulletSpawn");
		rb = GetComponent<Rigidbody2D> ();
	}
	
	// Update is called once per frame
	void FixedUpdate () 
	{
		delay += Time.deltaTime;

		transform.position += new Vector3 (Input.GetAxis  ("Horizontal"), 0, 0) * speed * Time.deltaTime;

		if(Input.GetButtonDown ("Jump"))
		{
			rb.AddForce(Vector2.up * jumpSpeed);
		}

		Vector3 pos = Camera.main.WorldToScreenPoint (transform.position);
		mousePos = Input.mousePosition - pos;
		angle = Mathf.Atan2 (mousePos.y, mousePos.x) * Mathf.Rad2Deg;
		shoulder.transform.rotation = Quaternion.AngleAxis (angle, Vector3.forward);

		if(Input.GetButton ("Fire1") && delay >= .5f)
		{
			delay = 0f;
			Rigidbody2D rb;
			GameObject bulletHandler;
			bulletHandler = Instantiate (bullet, bulletSpawn.transform.position, bulletSpawn.transform.rotation) as GameObject;
			rb = bullet.GetComponent<Rigidbody2D> ();
			rb.velocity = bulletSpawn.transform.forward * bulletSpeed;
		}

	}	               
}

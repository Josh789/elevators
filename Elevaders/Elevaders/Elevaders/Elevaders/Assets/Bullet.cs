﻿using UnityEngine;
using System.Collections;

public class Bullet : MonoBehaviour 
{

	public float bulletSpeed = 20f;
	public float timer;

	private Player player;
	private Rigidbody2D rb;
	private Enemy enemy;

	// Use this for initialization
	void Awake () 
	{
		player = GameObject.Find ("Player").GetComponent<Player> ();
		rb = GetComponent<Rigidbody2D> ();
	}

	void Update()
	{
		rb.AddForce (player.bulletSpawn.transform.right * bulletSpeed);
		timer += Time.deltaTime;

		if (timer >= 2f) 
		{
			Destroy (gameObject);
		}
	}

	void OnCollisionEnter2D(Collision2D other)
	{
		Debug.Log (other.gameObject.tag);

		if (other.gameObject.tag == "Enemy") 
		{
			//Destroy (rb);
			//other.gameObject.GetComponent<Rigidbody2D>().AddForce ((player.bulletSpawn.transform.right * bulletSpeed) * -1f);
			bulletSpeed = 0f;
			enemy = other.gameObject.GetComponent<Enemy> ();
			enemy.TakeDamage (20f);
		}
		else if(!other.gameObject.CompareTag ("Player"))
		{
			bulletSpeed = 0f;
			new WaitForSeconds(1);
			Destroy (gameObject);
		}
	}
}

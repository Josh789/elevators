﻿using UnityEngine;
using System.Collections;

public class Enemy : MonoBehaviour 
{
	public float health;

	// Use this for initialization
	void Awake () 
	{
		health = 100f;
	}
	
	// Update is called once per frame
	void Update () 
	{
		if (health <= 0f) 
		{
			Destroy (gameObject);
		}
	}

	public void TakeDamage(float damage)
	{
		health -= damage;
		Debug.Log (damage);
	}

}

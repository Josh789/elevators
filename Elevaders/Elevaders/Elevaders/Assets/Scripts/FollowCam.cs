﻿using UnityEngine;
using System.Collections;

public class FollowCam : MonoBehaviour 
{

	public Rigidbody2D player;

	// Use this for initialization
	void Awake () 
	{
	
	}
	
	// Update is called once per frame
	void Update () 
	{
		transform.position = player.transform.position + new Vector3 (0, 1, -10);
	}
}

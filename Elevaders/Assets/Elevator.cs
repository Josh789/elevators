﻿using UnityEngine;
using System.Collections;

public class Elevator : MonoBehaviour 
{
	public float distance;
	public float speed;
	public bool done;
	public Vector3 startPosition;

	void Start()
	{
		startPosition = transform.position;
	}

	void OnCollisionStay2D(Collision2D other)
	{
		if(other.gameObject.CompareTag("Player"))
		{
			Raise ();
			done = false;
		}
	}

	void OnCollisionExit2D(Collision2D other)
	{
		if(other.gameObject.CompareTag("Player"))
		{
			transform.position = startPosition;
			done = true;
		}
	}

	void OnTriggerEnter2D(Collider2D other)
	{
		if(other.CompareTag("Bound"))
		{
			Debug.Log ("Bound");

			speed = 0f;
		}
	}

	public void Lower()
	{
		transform.position = new Vector3 (0, distance, 0f);

		if (done) 
		{
			Raise ();
		}
	}

	public void Raise()
	{
		transform.position += Vector3.up * speed * Time.deltaTime;
	}

}

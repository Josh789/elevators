﻿using UnityEngine;
using System.Collections;

public class EnemyHealth : MonoBehaviour 
{

	public float health;

	// Use this for initialization
	void Awake () 
	{
		
	}
	
	// Update is called once per frame
	public void TakeDamage (float damage) 
	{
		health -= damage;

		if (health <= 0f && gameObject.CompareTag("Enemy")) 
		{
			Destroy (gameObject);
		}
	}
}

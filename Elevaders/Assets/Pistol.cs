﻿using UnityEngine;
using System.Collections;

public class Pistol : Gun
{

	public override void PrimaryFire()
	{
		if (delay >= .6f && ammo > 0f) 
		{
			Shoot (10f);
		}
	}

	public override void AltFire()
	{
		if (delay >= 1.2f && ammo >0f) 
		{
			Shoot (30f);
		}
	}
}

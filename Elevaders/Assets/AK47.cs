﻿using UnityEngine;
using System.Collections;

public class AK47 : Gun 
{

	void Awake()
	{
	}

	public override void PrimaryFire()
	{
		if (delay >= .6f && ammo > 0f) 
		{
			Shoot (20f);
		}
	}

	public override void AltFire()
	{
		if (delay >= .2f && ammo >0f) 
		{
			Shoot (5f);
		}
	}
}

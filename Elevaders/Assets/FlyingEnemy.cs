﻿using UnityEngine;
using System.Collections;

public class FlyingEnemy : MonoBehaviour
{
	public GameObject target;
	public float speed;

	private Player player;
	private float angle;
	private bool playerDetected;
	private float health;
	private AudioSource sound;

	// Use this for initialization
	void Awake() 
	{
		target = GameObject.Find ("Player");
		player = target.GetComponent<Player> ();
		health = 20f;
		sound = GetComponent<AudioSource>();
	}
	
	// Update is called once per frame
	void Update () 
	{
		

		if (playerDetected) 
		{
			Vector3 vectorToTarget = target.transform.position - transform.position;
			angle = Mathf.Atan2 (vectorToTarget.y, vectorToTarget.x) * Mathf.Rad2Deg;
			Quaternion q = Quaternion.AngleAxis (angle, Vector3.forward);
			transform.rotation = Quaternion.Slerp (transform.rotation, q, Time.deltaTime * 20f);

			transform.position += new Vector3 (vectorToTarget.x, vectorToTarget.y, 0).normalized * speed * Time.deltaTime;
			target = GameObject.Find ("Player").gameObject;
		}
	}

	void OnTriggerEnter2D(Collider2D other)
	{
		if (other.CompareTag ("Player")) 
		{
			playerDetected = true;
			target = GameObject.Find("Player").gameObject;
		}
	}

	void OnCollisionEnter2D(Collision2D other)
	{
		if(other.gameObject.CompareTag("Player"))
		{
			sound.Play ();
			player.TakeDamage (20);
			Destroy (gameObject);
		}
	}

	void TakeDamage(float damage)
	{
		health -= damage;
		if (health <= 0) 
		{
			Destroy (gameObject);
		}
	}
}

﻿using UnityEngine;
using System.Collections;

public class EnemyBullet : MonoBehaviour {

	public float bulletSpeed = 100f;
	public float timer;
	public bool isLeft;

	private Player player;
	private Rigidbody2D rb;


	// Use this for initialization
	void Awake () 
	{
		if (isLeft) 
		{
			FireLeft ();
		}
		else
		{
			FireRight();
		}
	}

	void Update()
	{
		timer += Time.deltaTime;

		if (timer >= 1f) 
		{
			Destroy (gameObject);
		}
	}

	void OnCollisionEnter2D(Collision2D other)
	{
		if (other.gameObject.tag == "Player") {
			//Destroy (rb);
			//other.gameObject.GetComponent<Rigidbody2D>().AddForce ((player.bulletSpawn.transform.right * bulletSpeed) * -1f);
			float damage = 20f;
			bulletSpeed = 0f;
			player = other.gameObject.GetComponent<Player> ();
			player.TakeDamage (damage);
			Destroy (gameObject);
		} else if (other.gameObject.CompareTag ("Platform")) {
			bulletSpeed = 0f;
			new WaitForSeconds (2);
			Destroy (gameObject);
		} else if (other.gameObject.CompareTag ("Enemy") || other.gameObject.CompareTag("Boss")) {
			Physics2D.IgnoreCollision(GetComponent<Collider2D>(), GetComponent<Collider2D>());
		}
			
	}

	void FireLeft()
	{
		rb = GetComponent<Rigidbody2D> ();
		rb.AddForce (Vector3.left * bulletSpeed);
	}

	void FireRight()
	{
		rb = GetComponent<Rigidbody2D> ();
		rb.AddForce (Vector3.right * bulletSpeed);
	}
}

﻿using UnityEngine;
using System.Collections;

public class Turret : MonoBehaviour
{
	public Transform bulletSpawn;
	public EnemyBullet bullet;
	public bool playerDetected;
	public bool isLeft;

	private float delay;

	// Use this for initialization
	void Awake () 
	{
	
	}
	
	// Update is called once per frame
	void Update () 
	{
		delay += Time.deltaTime;
			
		if (playerDetected && delay > 1f) 
		{
			Shoot ();
		}
	}

	void OnTriggerEnter2D(Collider2D other)
	{
		if (other.CompareTag ("Player")) 
		{
			playerDetected = true;
		}
	}

	void OnTriggerExit2D(Collider2D other)
	{
		if (other.CompareTag ("Player")) 
		{
			playerDetected = false;
		}
	}

	void Shoot()
	{
		delay = 0f;
		GameObject bulletHandler;
		bulletHandler = Instantiate (bullet, bulletSpawn.position, bulletSpawn.rotation) as GameObject;
		bullet.isLeft = isLeft;
	}
}

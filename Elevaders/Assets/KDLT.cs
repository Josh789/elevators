﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class KDLT : MonoBehaviour 
{

	public bool playerDetected;
	public GameObject bossBar;
	
	private Player player;
	private GameManager gameManager;
	private GameObject target;
	private EnemyHealth enemyHealth;
	private float health;

	// Use this for initialization
	void Awake() 
	{
		gameManager = GameObject.Find ("GameManager").GetComponent<GameManager> ();
		player = GameObject.Find ("Player").GetComponent<Player> ();
		enemyHealth = GetComponent<EnemyHealth> ();
	}
	
	// Update is called once per frame
	void Update () 
	{
		health = enemyHealth.health;

		if (health <= 0f) 
		{
			GetComponentInChildren<SpriteRenderer> ().enabled = false;
			gameManager.LevelWin();
			bossBar.SetActive (false);
			Destroy (gameObject);
		}

		if (playerDetected) 
		{
			bossBar.SetActive (true);
			gameManager.TextFlash ("BOSS FIGHT");
		}
	}

	void OnTriggerEnter2D(Collider2D other)
	{
		if (other.CompareTag ("Player")) 
		{
			playerDetected = true;
			target = other.gameObject;
		}
	}

	void OnCollisionStay2D(Collision2D other)
	{
		if(other.gameObject.CompareTag("Player"))
		{
			player.TakeDamage(20f);
		}
	}
}

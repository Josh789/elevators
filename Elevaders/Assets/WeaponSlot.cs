﻿using UnityEngine;
using System.Collections;

public class WeaponSlot : MonoBehaviour {

	void OnCollisionEnter2D(Collision2D other)
	{
		if (other.gameObject.CompareTag ("Player"))
		{
			Physics2D.IgnoreCollision (GetComponent<Collider2D> (), GetComponent<Collider2D> ());
		}
	}
}

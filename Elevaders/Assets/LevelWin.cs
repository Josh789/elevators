﻿using UnityEngine;
using System.Collections;

public class LevelWin : MonoBehaviour 
{
	public GameManager gameManager;

	void OnTriggerEnter2D(Collider2D other)
	{
		gameManager.LevelWin ();
	}
}

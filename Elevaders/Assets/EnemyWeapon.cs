﻿using UnityEngine;
using System.Collections;

public class EnemyWeapon : MonoBehaviour 
{

	public EnemyBullet enemyBullet;
	public GameObject bulletSpawn;
	public bool playerDetected;

	// Use this for initialization
	void Awake () 
	{
	
	}
	
	// Update is called once per frame
	void Update () 
	{
		if (playerDetected) 
		{
			Shoot ();
		}
	}

	void Shoot()
	{
		Instantiate (enemyBullet, bulletSpawn.transform.position, bulletSpawn.transform.localRotation);
		Rigidbody2D rb = enemyBullet.GetComponent<Rigidbody2D> ();
	}
}

﻿using UnityEngine;
using System.Collections;

public class DetectLeft : MonoBehaviour 
{

	public Enemy enemy;

	// Use this for initialization
	void Awake () 
	{
		enemy = GetComponentInParent<Enemy> ();	
	}
	
	// Update is called once per frame
	void OnTriggerStay2D (Collider2D other) 
	{
		if(other.CompareTag("Player"))
		{
			enemy.dir = -180;
		}
	}
}

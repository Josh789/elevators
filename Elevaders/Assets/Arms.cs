﻿using UnityEngine;
using System.Collections;

public class Arms : MonoBehaviour {

	// Update is called once per frame
	void Update () 
	{
		if (transform.rotation.z >= 90 && transform.rotation.z < 270f)
			transform.Rotate (new Vector3(180, 0, 0));
	}

	void OnCollisionStay2D(Collision2D other)
	{
			Physics2D.IgnoreCollision (GetComponent<Collider2D>(), GetComponent<Collider2D>());
	}

}

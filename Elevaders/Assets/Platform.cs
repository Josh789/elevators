﻿using UnityEngine;
using System.Collections;

public class Platform : MonoBehaviour 
{
	void FixedUpdate () 
	{
		int pos = Random.Range (-1, 1);
		float wobble = Random.value * Random.Range(-1,1);
		transform.position += new Vector3 (0, wobble, 0f);
	}
}

﻿using UnityEngine;
using System.Collections;

public class KillingFloor : MonoBehaviour 
{

	void OnTriggerEnter2D(Collider2D other)
	{
		other.GetComponent<Player>().TakeDamage(500000f);
	}
}

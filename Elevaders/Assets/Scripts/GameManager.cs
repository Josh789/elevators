﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections;
using System.Collections.Generic;

public class GameManager : MonoBehaviour 
{
	public float health;
	public float ammo;
	public Text missionText;
	public GameObject bossBar;
	public Elevator elevator;
	public GameObject checkpoint;
	public GameObject playerSpawn;
	public int lives;
	public Text lifeText;
	public GameObject player;
	public Text ammoText;
	public Slider healthBar;

	private float timer;
	private Player playerScript;
	private EnemyHealth bossHealth;
	private FollowCam followCam;
	private GameObject[] elevators;

	// Use this for initialization
	void Start () 
	{
		lives = 3;
		playerScript = GameObject.Find ("Player").GetComponent<Player> ();
		bossHealth = GameObject.Find ("Boss").GetComponent<EnemyHealth> ();
		followCam = GameObject.Find ("Main Camera").GetComponent<FollowCam> ();
		lifeText = GameObject.Find ("livesText").GetComponent<Text> ();
		TextFlash ("WASD to move, mouse to aim and fire");
	}
	
	// Update is called once per frame
	void Update () 
	{
		lifeText.text = "Lives: " + lives.ToString ();
		ammoText.text = "AMMO: " + playerScript.gun.ammo.ToString();
		healthBar.value = playerScript.health;
		bossBar.GetComponent<Slider> ().value = bossHealth.health;

		if (bossHealth.health <= 0f) 
		{
			if (elevator.done == false) 
			{
				LevelWin ();
			}
			bossBar.SetActive (false);
		}

		if (health <= 0f) 
		{
			healthBar.GetComponentInChildren<Image>().enabled = false;
		}

	}

	public void TextFlash(string text)
	{
		timer = 0f;
		timer += Time.deltaTime;
		missionText.text = text;

		missionText.enabled = true;

		if (timer >= 2f)
		{
			missionText.gameObject.SetActive(false);
		}

		if (timer >= 4f)
		{
			missionText.gameObject.SetActive(true);
		}

		if (timer >= 6f)
		{
			missionText.gameObject.SetActive(false);
		}
	}

	public void LevelWin()
	{
		SceneManager.LoadScene (SceneManager.GetActiveScene().buildIndex + 1);
	}

	public void Respawn()
	{
		player.transform.position = checkpoint.transform.position + Vector3.up;
		playerScript.health = 100f;
		lives -= 1;
		lifeText.text = "Lives: " + lives.ToString ();
	}

	public void GameOver()
	{
		LoadLevel.LoadScene (0);
		//TextFlash ("Game Over");
	}

}

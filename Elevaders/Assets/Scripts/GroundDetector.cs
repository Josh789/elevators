﻿using UnityEngine;
using System.Collections;

public class GroundDetector : MonoBehaviour 
{
		
	private Player player;

	// Use this for initialization
	void Awake () 
	{
		player = GetComponentInParent<Player> ();
	}
	
	void OnCollision2DStay(Collider2D other)
	{
		if(other.gameObject.CompareTag ("Ground"))
		{
			Debug.Log ("Working");
			player.isGround = true;
		}
	}
	
	void OnCollision2DExit(Collider2D other)
	{
		player.isGround = false;
	}
}

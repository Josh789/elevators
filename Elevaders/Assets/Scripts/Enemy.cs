﻿using UnityEngine;
using System.Collections;

public class Enemy : MonoBehaviour 
{
	public float health;
	public bool playerDetected;
	public GameObject target;
	public GameObject bullet;
	public Transform bulletSpawn;
	public float move;
	public float dir;

	private float delay;
	private EnemyBullet enemyBullet;
	private HingeJoint2D shoulder;
	private GameObject player;
	private float abs;


	// Use this for initialization
	void Awake () 
	{
		health = 100f;
		move = 1f;
		shoulder = GetComponentInChildren<HingeJoint2D> ();
		player = GameObject.Find ("Player");
		target = player;
		dir = 1;
	}
	
	// Update is called once per frame
	void Update () 
	{
		delay += Time.deltaTime;

		if (health <= 0f) 
		{
			Destroy (gameObject);
		}

		if (playerDetected && delay >= 1f) 
		{
			Attack (target);
		}

		if (target != null) 
		{
			move = 0f;
		}
		transform.position += new Vector3 (move, 0f, 0f) * Time.deltaTime;

		transform.Rotate(new Vector3 (0, dir, 0) * Time.deltaTime);
	}

	public void TakeDamage(float damage)
	{
		health -= damage;
	}

	void Attack(GameObject target)
	{	delay = 0f;
		Rigidbody2D rb;
		GameObject bulletHandler;
		bulletHandler = Instantiate (bullet, bulletSpawn.position, bulletSpawn.rotation) as GameObject;
		rb = bullet.GetComponent<Rigidbody2D> ();
		enemyBullet = GetComponent<EnemyBullet> ();
	}

	void OnTriggerEnter2D(Collider2D other)
	{
		if (other.CompareTag ("Bound")) 
		{
			move *= -1f;
		}
	}

	void OnTriggerStay2D(Collider2D other)
	{
		if(other.CompareTag("Player") && delay >= 1f)
		{
			playerDetected = true;
			Attack(other.gameObject);
		}
	}

}

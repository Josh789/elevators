﻿using UnityEngine;
using System.Collections;

public class Bullet : MonoBehaviour 
{

	public float bulletSpeed = 20f;
	public float timer;
	public float damage;

	private Player player;
	private Rigidbody2D rb;
	private EnemyHealth enemy;

	// Use this for initialization
	void Awake () 
	{
		player = GameObject.Find ("Player").GetComponent<Player> ();
		rb = GetComponent<Rigidbody2D> ();
		rb.AddForceAtPosition (player.bulletSpawn.right * bulletSpeed, transform.position);
	}

	void Update()
	{
		timer += Time.deltaTime;

		if (timer >= 2f) 
		{
			Destroy (gameObject);
		}
	}

	void OnCollisionEnter2D(Collision2D other)
	{
		if (other.gameObject.tag == "Enemy" || other.gameObject.tag == "Boss") {
			bulletSpeed = 0f;
			enemy = other.gameObject.GetComponent<EnemyHealth> ();
			enemy.TakeDamage (damage);
			Destroy (gameObject);
		} else if (other.gameObject.CompareTag ("Platform")) {
			bulletSpeed = 0f;
			new WaitForSeconds (2);
			Destroy (gameObject);
		} else if (other.gameObject.CompareTag ("Player")) 
		{
			Physics2D.IgnoreCollision (GetComponent<Collider2D> (), GetComponent <Collider2D> ());
		}
	}
}

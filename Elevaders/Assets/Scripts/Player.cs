﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Player : MonoBehaviour 
{
	public float speed;
	public float jumpSpeed;
	public bool isGround;
	public GameObject bullet;
	public float bulletSpeed = 20000f;
	public Transform bulletSpawn;
	public Slider healthBar;
	public float health;
	public GameObject weaponSlot;
	public Gun gun;
	public float sprintSpeed;

	private HingeJoint2D shoulder;
	private float angle;
	private float delay;
	private Vector3 mousePos;
	private GameObject arm;
	private Rigidbody2D rb;
	private float h;
	private float v;
	private bool isLadder;
	private bool kinematic;
	private float key;
	private float gameHealth;
	public GameManager gameManager;
	private Animator anim;
	private bool isPortal;
	public float stamina;
	private bool isSprinting;


	// Use this for initialization
	void Awake () 
	{
		shoulder = GetComponentInChildren<HingeJoint2D> ();
		arm = GameObject.Find ("Arms");
		rb = GetComponent<Rigidbody2D> ();
		health = 100f;
		healthBar = GameObject.Find ("healthBar").GetComponent<Slider> ();
		anim = GetComponent<Animator> ();
		stamina = 100f;
	}
	
	// Update is called once per frame
	void FixedUpdate () 
	{
		h = Input.GetAxis ("Horizontal");
		v = Input.GetAxis ("Vertical");

		if (Input.GetAxis ("Horizontal") != 0) {
			anim.SetBool ("IsWalking", true);
		} else {
			anim.SetBool ("IsWalking", false);
		}

		delay += Time.deltaTime;

		if (!isSprinting && stamina < 100f) 
		{
			stamina += 5f * Time.deltaTime;
		}

		rb.isKinematic = kinematic;

		switch(isLadder)
		{
		case true:
			kinematic = true;
			transform.position += new Vector3 (h, v, 0f) * 7 * Time.deltaTime;
			break;

		case false:
			kinematic = false;
			//rb.velocity = (Vector2.right * h * speed * Time.deltaTime);
			rb.AddForce(new Vector2(h, 0f) * speed * Time.deltaTime, ForceMode2D.Impulse);
			break;
		}

		if(Input.GetButtonDown ("Jump") && isGround == true)
		{
			rb.velocity += (Vector2.up * jumpSpeed * Time.deltaTime);
			anim.SetBool ("IsJumping", true);
		}

		Vector3 pos = Camera.main.WorldToScreenPoint (transform.position);
		mousePos = Input.mousePosition - pos;
		angle = Mathf.Atan2 (mousePos.y, mousePos.x) * Mathf.Rad2Deg;
		shoulder.transform.rotation = Quaternion.AngleAxis (angle, Vector3.forward);

		if(Input.GetButton ("Fire1"))
		{
			gun.PrimaryFire ();
		}

		if (Input.GetButton ("Fire2")) 
		{
			gun.AltFire ();
		}

		/*if (angle >= 90f && angle < 270f) 
		{
			transform.rotation = Quaternion.Euler (new Vector3 (0, 180, 0));
			//arm.transform.rotation *= Quaternion.Euler (new Vector3 (0, 180, 0));
		}

		if (angle <= 90f || angle > 270f) 
		{
			transform.rotation = Quaternion.Euler (new Vector3 (0, 0, 0));
			//arm.transform.rotation *= Quaternion.Euler (new Vector3 (0, 0, 0));
		}*/

		if (Input.GetButton ("Sprint") && stamina > 0f) {
			speed = sprintSpeed;
			isSprinting = true;
			stamina -= 20f * Time.deltaTime;
		} else {
			speed = 60f;
			isSprinting = false;
		}

	}

	void OnTriggerEnter2D(Collider2D other)
	{

		if (other.CompareTag ("Ladder"))
		{
			isLadder = true;
			isGround = true;
			anim.SetBool ("IsJumping", false);
		}

		if (other.CompareTag ("Key")) 
		{
			key += 1;
			Destroy (other.gameObject);
		}

		if (other.CompareTag("Door") && key > 0)
		{
			Destroy (other.gameObject);
			key -= 1f;
		}

		if (other.CompareTag ("Player")) 
		{
			Physics2D.IgnoreCollision (GetComponent<Collider2D> (), GetComponent<Collider2D> ());
		}

		if (other.CompareTag ("HealthPU") && health < 100f) 
		{
			health += 10f;
			Destroy (other.gameObject);
			gameManager.health = health;
		}

		if (other.CompareTag ("AmmoPU")) 
		{
			gun.ammo += 50f;
			Destroy (other.gameObject);
		}

		if (other.CompareTag ("CP")) 
		{
			gameManager.TextFlash ("CHECKPOINT");
			gameManager.checkpoint = other.gameObject;
			health = 100f;
		}

		if (other.CompareTag ("LifePU")) 
		{
			gameManager.lives += 1;
			Destroy (other.gameObject);
		}

	}

	void OnTriggerExit2D(Collider2D other)
	{
		if (other.CompareTag ("Ladder")) 
		{
			isLadder = false;
		}

		if (other.CompareTag ("Platform")) 
		{
			isGround = false;
		}
	}

	void OnTriggerStay2D(Collider2D other)
	{
		if(other.CompareTag("Gun"))
		{
			if(Input.GetButtonDown("Action"))
			{
				foreach (Transform child in weaponSlot.transform) 
				{
					child.GetComponent<BoxCollider2D> ().enabled = true;
					child.transform.parent = null;
				}
				SwitchWeapon(other.gameObject);
			}

		}

		if (other.CompareTag ("Platform") || other.CompareTag("Elevator")) 
		{
			isGround = true;
			anim.SetBool ("IsJumping", false);
		}

		if(other.CompareTag("Portal"))
		{
			if (Input.GetButtonDown ("Action")) 
			{
				transform.position = other.gameObject.GetComponent<Portal> ().partner.position;
			}
		}
	}

	void OnCollisionStay2D(Collision2D other)
	{
		if (other.gameObject.CompareTag ("Player")) 
		{
			Physics2D.IgnoreCollision (GetComponent<Collider2D> (), GetComponent<Collider2D> ());
		}
	}

	public void TakeDamage(float damage)
	{
		health -= damage;
		gameManager.health = health;
		if (health <= 0f && gameManager.lives > 0)
		{
			Debug.Log ("Dead Bitch");
			gameManager.Respawn ();
		}

		if (health <= 0f && gameManager.lives < 1) 
		{
			Destroy (gameObject);
			gameManager.GameOver ();
		}
	}

	/*public void Shoot ()
	{
		delay = 0f;
		Rigidbody2D bulletRB;
		GameObject bulletHandler;
		bulletHandler = Instantiate (bullet, bulletSpawn.position, bulletSpawn.rotation) as GameObject;
		bulletRB = bullet.GetComponent<Rigidbody2D> ();
		bulletRB.velocity = bulletSpawn.forward * bulletSpeed;
		ammo -= 1f;
	}*/

	public void SwitchWeapon(GameObject newweapon)
	{
		gun = WeaponCheck (newweapon.GetComponent<Gun>());
		gun.transform.SetParent (weaponSlot.transform);
		gun.transform.localPosition = new Vector3 (0, 0, 0);
		gun.GetComponent<BoxCollider2D> ().enabled = false;
		gun.transform.localRotation = Quaternion.Euler(0,0,0);
		foreach (Transform child in gun.transform) 
		{
			if (child.name == "bulletSpawn") 
			{
				bulletSpawn = child;
			}
		}
	}

	public Gun WeaponCheck(Gun weapon)
	{
		if (weapon is AK47) 
		{
			AK47 newgun = (AK47)weapon;
			return newgun;
		}
		if (weapon is Pistol) 
		{
			Pistol newgun = (Pistol)weapon;
			return newgun;
		}
		return null;
	}
}

﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class DisplayText : MonoBehaviour 
{

	private Text missionText;
	private float timer;

	void Awake()
	{
		missionText = GetComponent<Text> ();
	}

	// Update is called once per frame
	void Update () 
	{
		timer += Time.deltaTime;

		if (timer >= 3f) 
		{
			missionText.enabled = true;
		}

		if (timer >= 8f) 
		{
			missionText.enabled = false;
		}

	}
}

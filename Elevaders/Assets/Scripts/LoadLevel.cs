﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

public static class LoadLevel 
{
 
	public static void LoadScene(int level)
	{
		SceneManager.LoadScene(level);
	}

	public static void ExitGame()
	{
		Application.Quit ();
	}
}

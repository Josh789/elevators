﻿using UnityEngine;
using System.Collections;

public class Gun : MonoBehaviour 
{
	public GameObject bullet;
	public Transform bulletSpawn;
	public float ammo;
	public Bullet bulletScript;
	public float bulletSpeed;
	public float delay;
	public AudioSource gunfire;
	public GameManager gameManager;
	// Use this for initialization
	void Awake () 
	{
		gameManager = GameObject.Find ("GameManager").GetComponent<GameManager> ();
		gameManager.ammo = ammo;
	}
	
	// Update is called once per frame
	void Update () 
	{
		delay += Time.deltaTime;
		gameManager.ammo = ammo;
	}

	public void Shoot (float damage)
	{
		delay = 0f;
		Rigidbody2D bulletRB;
		GameObject bulletHandler;
		bulletHandler = Instantiate (bullet, bulletSpawn.position, bulletSpawn.rotation) as GameObject;
		bulletRB = bullet.GetComponent<Rigidbody2D> ();
		bulletRB.velocity = bulletSpawn.forward * bulletSpeed;
		bulletScript.damage = damage;
		ammo -= 1f;
		gunfire.Play ();
	}

	public virtual void PrimaryFire()
	{
		Debug.Log ("Base");
	}

	public virtual void AltFire()
	{
	}

	void OnTriggerStay2D(Collider2D other)
	{
		Physics2D.IgnoreCollision(GetComponent<Collider2D>(), GetComponent<Collider2D>());
	}
}
